# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.3.5 - 2025-02-25(17:01:00 +0000)

## Release v2.3.4 - 2025-01-20(15:53:49 +0000)

### Other

- [CI] jfrog APT key expired

## Release v2.3.3 - 2025-01-08(15:39:47 +0000)

### Other

- Upload python packages of untagged versions

## Release v2.3.2 - 2024-12-30(11:08:16 +0000)

### Other


## Release v2.3.1 - 2024-12-17(23:19:46 +0000)

### Other


## Release v2.3.0 - 2024-12-17(14:18:32 +0000)

### Other

- Reduce unnecessary formatted writes
- Provide integer -> string conversion functions
- [libamxp]Memory leak can occur when signal can not be emitted
- [Amxc] Enable comparison of null variants
- [amxo-cg] not compiling with gcc13.3.0
- Reduce usage of appendf in expression parser
- Improve data model mamagement functions
- Device2 obsoleted and deprecated datamodel enumeration values should be ignored.
- Cache parsed mib expressions
- [Amxb] Always log bus timeouts to syslog
- Custom action handlers must be used when available in creating pcb reply messages
- [AMX] Slots callback functions are called multiple times
- [CI] code-style issues are strangely formatted

## Release v2.2.6 - 2024-12-09(11:40:14 +0000)

## Release v2.2.5 - 2024-12-03(12:01:14 +0000)

### Other

- amxc_set improvements
- [FSAM][Container] MQTT Events not dispatched on Device.MQTT.Client.
- add support for pon lines + update templates with more variables
- [Gitlab] Find an alternative to Remove all approvals when commits are added to the source branch

## Release v2.2.4 - 2024-12-03(02:44:28 +0000)

### Other


## Release v2.2.3 - 2024-11-29(10:01:28 +0000)

### Other

- [ubusd] handle large messages
- Add support for pon configuration of isam lines with sahtne scripts on jenkins

## Release v2.2.2 - 2024-11-21(20:21:32 +0000)

### Other

- Failed to load amx pcb backend in amxdev docker

## Release v2.2.1 - 2024-11-19(15:25:03 +0000)

### Other

- Fix sah_nte artifactory issues

## Release v2.2.0 - 2024-11-19(09:05:48 +0000)

### Other

- Update the TR-181 XML file with the latest 24.09.2024
- Update the key validation behaviour.
- Update the tr-181-2-usp-full XML to the latest version.
- [LCM] The first DU instance gets renamed (but not in dm)
- Optimizations in ambiorix libraries
- Add ninja to docker image building libxml2 CI

## Release v2.1.0 - 2024-11-06(11:39:12 +0000)

### Other

- Update Dockerfile by adding the lib usp

## Release v2.0.23 - 2024-11-05(10:38:58 +0000)

### Other


## Release v2.0.22 - 2024-09-17(13:09:22 +0000)

### Other


## Release v2.0.21 - 2024-09-16(14:48:02 +0000)

### Other


## Release v2.0.20 - 2024-09-10(11:45:34 +0000)

### Other

- - Integrate softathome toolchain for BRCM BSP bcm963xx 6.1.1

## Release v2.0.19 - 2024-08-28(12:47:15 +0000)

### Other


## Release v2.0.18 - 2024-08-07(11:47:31 +0000)

## Release v2.0.16 - 2024-08-02(12:31:58 +0000)

### Other

- - Create a test setup for CSSP + clear documentation
- Use separate and distinct release numbers for dev and official release \[new\]
- \[naming\]\[SAHTag\] Add support for dates in SAHTag \[new\]
- \[wrtconfigrepo\] Updating feed requires feed repo path to match feed name \[fix\]
- \[CI\] Replace CI_BUILD_REF_SLUG with CI_COMMIT_REF_SLUG
- Add support for native integration into feeds \[new\]
- Fix updating of feeds in WrtConfigRepo \[fix\]
- \[review.py\] Add the Review.get_commits() method
- Add support for four digit tags \[new\]
- Add retry module for methods most prone to gitlab 500 error
- Add errorhandling for functions that fail often \[other\]
- Add list pipeline method
- Use regex to update yocto-distro dependencies \[other\]
- Repositories under yocto-hgw/lcm/config are of the type of YoctoDistroRepo \[other\]
- \[naming.py\] Add support for gitlab.softathome.com mappings \[new\]
- \[user.py\] Add get_user_mail() function \[new\]
- Add sahlab functionality for creating mirror
- \[naming.py\] make name mappings compatible with multiple git spaces \[new\]
- add support for feeds in the profiles directory of a wrt config
- Add jira integration \[new\]
- Gitlab does not always return tags ordered by date \[fix\]
- Add support for integration from SAHRepo to ContainerRepo \[new\]
- \[issue.py\] Add method for setting status of Jira issues \[new\]
- \[JiraIssue\] Add support for linking issues \[new\]

## Release v2.0.15 - 2024-08-01(15:41:58 +0000)

### Other

- [artifactory] fix API kys in plain, rebase will follow
- [CI] ODL check broken - complains about libc

## Release v2.0.13 - 2024-07-25(12:31:56 +0000)

### Other

- - amx-validator: missing key checks
- [CI] Add targets to the build pipeline
- - AMX-Validator: Special Alias key check.
- Add exclude enum capabilities for the amx-validation script.
- Functional key check
- - amx-validator: add excluded list for accessType.
- [USP] Add obuspa license
- install yq command

## Release v2.0.12 - 2024-07-18(14:20:45 +0000)

### Other

- AMXMOP typecheck is failing

## Release v2.0.11 - 2024-07-04(07:48:35 +0000)

### Other

- [CI] Add targets to the build pipeline
- [CI] artifactory explorer misses libwebsockets package

## Release v2.0.10 - 2024-06-26(12:28:12 +0000)

## Release v2.0.9 - 2024-06-25(14:10:12 +0000)

## Release v2.0.8 - 2024-06-20(14:29:29 +0000)

## Release v2.0.7 - 2024-06-20(10:45:43 +0000)

## Release v2.0.6 - 2024-06-18(22:17:21 +0000)

### Other


## Release v2.0.5 - 2024-06-18(13:06:29 +0000)

### Other


## Release v2.0.4 - 2024-06-16(21:40:00 +0000)

### Other


## Release v2.0.3 - 2024-06-11(09:56:19 +0000)

### Other


## Release v2.0.2 - 2024-06-06(10:07:25 +0000)

### Other

- Waiting for events in pytest results in Segmentation fault (core dumped)

## Release v2.0.1 - 2024-06-06(09:47:12 +0000)

### Fixes

- Dockerfile: renew GITLAB_API_KEY in docker environment

## Release v2.0.0 - 2024-06-04(11:22:46 +0000)

### Breaking

- - Update the JFrog executable used in Artifactory

### Other

- - [WIFI7] First integration of QSDK ATH 12.4

## Release v1.5.1 - 2024-06-03(08:21:44 +0000)

### Other

- Print out total number of errors found
- ci: add IMAGE_PATH variable to correctly push opensource container

## Release v1.5.0 - 2024-05-23(09:44:38 +0000)

### Breaking

- - Update the Artifactory version for the JFrog executable used in Artifactory

### Other

- - Create a test setup for CSSP + clear documentation
- Add tools for performing TR106 syntax rule checks on AMX/ODL files
- Missing compilation flags during cross compilations
- [Regression] Old p4 tags in Component.dep no longer supported
- [CI] Fix: meson dependency of buildutils screws up the CFLAGS and LDFLAGS of opensource compilations
- [CI][services_lua] normalize_build_dir() function tries to move directory symlinks
- [code-quality] Make it possible to run FixLicenses locally

## Release v1.4.11 - 2024-05-02(15:01:47 +0000)

## Release v1.4.10 - 2024-05-02(09:23:36 +0000)

## Release v1.4.9 - 2024-03-11(11:12:20 +0000)

### Other


## Release v1.4.8 - 2024-03-11(10:55:26 +0000)

### Other


## Release v1.4.7 - 2024-02-28(07:44:02 +0000)

### Other

- add test and fix for partials failing the check

## Release v1.4.6 - 2024-02-08(17:47:28 +0000)

### Other

- Find an alternative to push rules

## Release v1.4.5 - 2024-01-25(15:53:34 +0000)

### Other

- PPP down on MOP08
- Add clearer message for ScanConfigOpts complaint

## Release v1.4.4 - 2024-01-12(13:19:52 +0000)

### Other


## Release v1.4.3 - 2023-12-20(13:20:27 +0000)

### Fixes

- Debug issue sahname packages

## Release v1.4.2 - 2023-11-20(17:06:43 +0000)

### Other


## Release v1.4.1 - 2023-10-27(07:30:13 +0000)

### Other


## Release v1.4.0 - 2023-10-26(13:41:28 +0000)

### New

- Add generate license on same branch job

### Other


## Release v1.3.21 - 2023-10-20(13:10:24 +0000)

### Other

- Modify scantree to allow IncludeNode inside SrcNode
- Add check for licenses

## Release v1.3.20 - 2023-09-18(12:50:07 +0000)

### Other

- add gitpython for merge request generation

## Release v1.3.19 - 2023-08-31(11:36:07 +0000)

### Other


## Release v1.3.18 - 2023-08-28(21:21:00 +0000)

### Other


## Release v1.3.17 - 2023-08-21(13:05:20 +0000)

### Other


## Release v1.3.16 - 2023-08-16(10:09:17 +0000)

### Other


## Release v1.3.15 - 2023-08-07(11:30:50 +0000)

## Release v1.3.14 - 2023-07-25(13:12:40 +0000)

### Other

- [CI] Replace CI_BUILD_REF_SLUG with CI_COMMIT_REF_SLUG
- Add libyajl-dev explicitly
- Resolve errors from new Pylint version

## Release v1.3.13 - 2023-07-12(10:12:24 +0000)

### Other


## Release v1.3.12 - 2023-07-07(11:21:13 +0000)

### Other


## Release v1.3.11 - 2023-07-06(07:53:51 +0000)

### Other


## Release v1.3.10 - 2023-06-22(11:43:04 +0000)

### Other

- [CI] Replace CI_BUILD_REF_SLUG with CI_COMMIT_REF_SLUG

## Release v1.3.9 - 2023-06-14(14:59:11 +0000)

## Release v1.3.8 - 2023-05-26(08:52:06 +0000)

### Other


## Release v1.3.7 - 2023-05-10(13:44:10 +0000)

### Other

- Modify remove_file_if_exists to work on directories
- Fix and optimize script used for filling the artifactory with packages for new cross-compilers

## Release v1.3.6 - 2023-05-05(12:39:21 +0000)

### Other

- Avoid overwriting cargo PATH

## Release v1.3.5 - 2023-04-26(07:03:02 +0000)

## Release v1.3.4 - 2023-04-21(07:34:00 +0000)

### Other

- Allow `.uc` files in the odl directory
- Create acl group with different group id
- Disable pylint

## Release v1.3.3 - 2023-04-06(09:17:03 +0000)

## Release v1.3.2 - 2023-02-16(14:34:14 +0000)

### Other

- [SID] Tests from version v1.3.0

## Release v1.3.1 - 2023-02-14(11:31:41 +0000)

### Fixes

- Pylint issues with python3 bindings tests

## Release v1.3.0 - 2023-02-13(15:38:12 +0000)

### New

- Add RUST Support in CI

### Fixes

- Fix Python3 amx bindings install target

### Other

- Change python3 bindings tests to comply with usp spec
- Add gitlabinternal to known_hosts

## Release v1.2.2 - 2023-02-01(15:25:53 +0000)

### Other


## Release v1.2.1 - 2023-02-01(03:05:23 +0000)

### Other


## Release v1.2.0 - 2023-01-26(16:08:47 +0000)

### New

- Add amxb_set_config support to python3 amx bindings

### Fixes

- [amx] Error when we try a second reboot

### Other


## Release v1.1.78 - 2023-01-12(09:34:41 +0000)

### Fixes

- Path to sut.dm.IP.Interface['3'].IPv4Address['1'].proxy() doenst work

## Release v1.1.77 - 2023-01-11(14:33:34 +0000)

### Fixes

- [amx] Unable to get data model object after a reboot

### Other


## Release v1.1.76 - 2023-01-10(10:09:31 +0000)

### Other


## Release v1.1.76 - 2023-01-10(10:07:25 +0000)

### Other


## Release v1.1.76 - 2023-01-10(10:05:37 +0000)

### Other


## Release v1.1.75 - 2022-12-20(00:52:47 +0000)

### Other


## Release v1.1.74 - 2022-12-06(14:48:39 +0000)

## Release v1.1.73 - 2022-12-05(16:02:13 +0000)

## Release v1.1.72 - 2022-12-02(15:45:12 +0000)

### Other


## Release v1.1.71 - 2022-12-02(12:25:07 +0000)

### Other


## Release v1.1.70 - 2022-11-30(10:51:56 +0000)

### Other


## Release v1.1.69 - 2022-11-29(11:05:06 +0000)

### Other


## Release v1.1.68 - 2022-11-20(00:39:19 +0000)

### Other


## Release v1.1.66 - 2022-11-08(01:32:06 +0000)

### Other


## Release v1.1.65 - 2022-11-04(21:53:41 +0000)

### Other


## Release v1.1.64 - 2022-11-04(15:06:05 +0000)

### Other


## Release v1.1.63 - 2022-10-27(14:38:13 +0000)

### Other


## Release v1.1.62 - 2022-10-25(11:47:27 +0000)

### Other


## Release v1.1.59 - 2022-10-12(10:08:26 +0000)

### Other

- Add acl group to container

## Release v1.1.58 - 2022-10-11(14:05:24 +0000)

## Release v1.1.57 - 2022-10-05(18:54:19 +0000)

### Other

- SSH session not active exception

## Release v1.1.56 - 2022-10-04(22:35:48 +0000)

### Other


## Release v1.1.55 - 2022-10-04(16:19:19 +0000)

### Other


## Release v1.1.54 - 2022-10-04(10:45:17 +0000)

### Other


## Release v1.1.53 - 2022-09-09(12:57:46 +0000)

### Other


## Release v1.1.52 - 2022-09-07(12:45:58 +0000)

### Other


## Release v1.1.51 - 2022-09-07(06:35:59 +0000)

## Release v1.1.50 - 2022-09-01(17:18:56 +0000)

### Fixes

- Issue: ambiorix/bindings/python3#14 Issues with datamodel during tests

### Other


## Release v1.1.49 - 2022-08-26(13:38:20 +0000)

## Release v1.1.48 - 2022-08-26(11:45:22 +0000)

## Release v1.1.47 - 2022-08-24(11:50:54 +0000)

### Other

- odl-check job fails due to empty odl file

## Release v1.1.46 - 2022-08-22(12:18:13 +0000)

## Release v1.1.45 - 2022-08-17(13:03:01 +0000)

### Other


## Release v1.1.44 - 2022-08-11(14:46:18 +0000)

### Other


## Release v1.1.43 - 2022-08-01(10:56:59 +0000)

### Other


## Release v1.1.42 - 2022-07-26(19:45:16 +0000)

### Other

- amx mop support for REQUIRES_WAN

## Release v1.1.41 - 2022-07-26(13:19:58 +0000)

### Other

- sysinit done optional

## Release v1.1.40 - 2022-07-18(13:10:35 +0000)

### Other


## Release v1.1.39 - 2022-07-15(12:54:00 +0000)

### Other


## Release v1.1.38 - 2022-07-15(09:19:28 +0000)

### Other


## Release v1.1.37 - 2022-07-12(13:14:49 +0000)

### Other


## Release v1.1.36 - 2022-07-12(07:57:06 +0000)

### Other

- Revert "kill pytest after 15 minutes"

## Release v1.1.35 - 2022-07-11(13:16:05 +0000)

### Other

- USP needs async userflags for functions

## Release v1.1.34 - 2022-07-04(14:23:05 +0000)

### Other


## Release v1.1.33 - 2022-07-04(13:28:34 +0000)

## Release v1.1.32 - 2022-06-29(14:09:23 +0000)

## Release v1.1.31 - 2022-06-29(08:11:44 +0000)

### Other


## Release v1.1.30 - 2022-06-28(12:00:53 +0000)

### Fixes

- tests_tr181-time is stuck on setting mininet node's IP with DHCP

## Release v1.1.29 - 2022-06-27(12:30:33 +0000)

### Other

- reduce the number of errors in case of WAN failure

## Release v1.1.28 - 2022-06-23(12:48:13 +0000)

## Release v1.1.27 - 2022-06-16(12:02:34 +0000)

### Other

- reduce the number of errors in case of WAN failure

## Release v1.1.26 - 2022-06-15(07:08:47 +0000)

### Other


## Release v1.1.25 - 2022-06-14(10:50:36 +0000)

## Release v1.1.24 - 2022-05-25(14:04:27 +0000)

### Other


## Release v1.1.23 - 2022-05-25(10:15:14 +0000)

### Other


## Release v1.1.22 - 2022-05-18(13:05:00 +0000)

### Fixes

- Issue: ambiorix/bindings/python3#13 Improve error message when function call fails

### Other


## Release v1.1.21 - 2022-05-18(10:29:11 +0000)

### Other


## Release v1.1.20 - 2022-05-17(11:03:30 +0000)

## Release v1.1.19 - 2022-05-10(13:19:13 +0000)

## Release v1.1.18 - 2022-05-10(12:42:30 +0000)

### Other

- Update to latest opensource pcb versions

## Release v1.1.17 - 2022-05-06(15:15:33 +0000)

### Fixes

- revert dependency problem

## Release v1.1.16 - 2022-05-06(14:15:40 +0000)

## Release v1.1.15 - 2022-04-29(12:54:05 +0000)

### Other

- Upstep pcb versions

## Release v1.1.14 - 2022-03-28(11:28:41 +0000)

### Fixes

- Installing deps of deps can result in wrong versions

## Release v1.1.12 - 2022-03-07(07:52:40 +0000)

### Fixes

- Upstep of sah-services-pcb-ser-ddw-dev breaks pcb-docgen

## Release v1.1.8 - 2022-02-21(10:02:57 +0000)

### Other

- Upstep sah-ci dependency

## Release v1.1.6 - 2022-02-10(12:19:07 +0000)

