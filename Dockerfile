FROM registry.gitlab.com/soft.at.home/docker/sah-ci:v3.2.1
ARG VERSION
ARG BUILD_DATE

## Install base dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt install -y \
    atftpd \
    bridge-utils \
    cmake \
    iperf \
    iptables \
    isc-dhcp-client \
    socat \
    libjson-c-dev \
    libevent-dev \
    libxslt1.1 \
    lua5.1 \
    liblua5.1-dev \
    minicom \
    mosquitto \
    mosquitto-clients \
    netcat \
    net-tools \
    openssh-server \
    screen \
    syslog-ng \
    tcpdump \
    telnet \
    screen \
    socat \
    valgrind \
    cloc \
    libcairo2-dev \
    iputils-arping \
    sshpass \
    && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

RUN pip3 install --upgrade pip

# General Python 3 packages
RUN pip3 install \
    cryptography==2.8 \
    paramiko \
    pySerial \
    pyyaml \
    requests \
    robotframework \
    scp \
    python-jenkins \
    pytest-incremental \
    pytest-timeout==2.1.0 \
    ssh2-python \ 
    pytest==7.1.3 \
    pytest-metadata==2.0.4

ENV CONTAINER_VERSION ${BUILD_DATE}_${VERSION}
ENV PCB_SYS_BUS "pcb://ipc:[/tmp/local]"
ENV LUA_PATH "/usr/local/share/lua/5.3/?.lua;/usr/local/share/lua/5.3/?/init.lua;/usr/local/lib/lua/5.3/?.lua;/usr/local/lib/lua/5.3/?/init.lua;/usr/lib/lua/?.lua;./?.lua;./?/init.lua"
ENV LUA_CPATH "/usr/local/lib/lua/5.3/?.so;/usr/local/lib/lua/5.3/lib/loadall.so;./?.so;/usr/lib/lua/?.so"

COPY resources/init.sh /usr/local/bin/

ENV LD_LIBRARY_PATH /usr/local/lib

RUN cd ~

RUN git clone https://git.openwrt.org/project/libubox.git && \
    cd libubox && \ 
    git checkout 75a3b870cace1171faf57bd55e5a9a2f1564f757 && \ 
    cmake . && make && make install

RUN git clone https://git.openwrt.org/project/ubus.git && \
    cd ubus && \
    git checkout f787c97b34894a38b15599886cacbca01271684f && \
    cmake . && make && make install && \
    cd .. && rm -rf libubox ubus && \
    mkdir -p /var/run/ubus

RUN sudo ldconfig

RUN export PKG_CONFIG_PATH=/usr/share/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib/pkgconfig && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-app.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-bus.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-cli.git && \
    export STAGINGDIR=/ && \
    cd pcb-app && export INSTALL=install && make compile && make install && \
    cd ../pcb-cli && make compile && make install && \
    cd ../pcb-bus && make compile && make install && \
    cd .. && rm -rf pcb-app pcb-bus pcb-cli


ENTRYPOINT ["/usr/bin/dumb-init","--","init.sh"]
